function hello() {
    var imie = prompt("Podaj swoje imię użytkowniku!!!");
    var anonim = "A więc witaj użytkowniku anonimowy.";
    var info = "<br> Życzę miłego surfowania!";
    var user = "Hej kolego <br>";

    if (imie == "") {
        document.getElementById('name').innerHTML = anonim + info;
    } else {
        document.getElementById('name').innerHTML = user + imie + info;
    }
}

function messageSent() {
    var name = document.getElementById('name').value;
    var surname = document.getElementById('surname').value;
    var message = "Wiadomosc zostala wyslana ";
    var notFilled = "Wypelnij poprawnie formularz";
    if ((name == "") || (surname == "")) {
        document.getElementById('informacja').innerHTML = notFilled;

    } else {
        document.getElementById('informacja').innerHTML = name + " " + surname + " " + message;
    }
}